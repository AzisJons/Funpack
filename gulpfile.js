'use strict';

var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var gzip = require('gulp-gzip');
var gunzip = require('gulp-gunzip');
var sourcemaps = require('gulp-sourcemaps');

const AUTOPREFIXER_BROWSERS = [
  'ie >= 11',
  'ff >= 40',
  'chrome >= 40',
  'safari >= 7',
  'opera >= 30',
];

gulp.task('css:min', function () {
  return gulp.src('./public/scss/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'nested',
      precision: 10,
      includePaths: ['.'],
      onError: console.error.bind(console, 'Sass error:')
    }))
    .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
    .pipe(csso())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./public/css'));
});

gulp.task('css:gzip', function() {
  return gulp.src('./public/css/*.css')
    .pipe(gzip())
    .pipe(gulp.dest('./public/css'));
});

gulp.task('css', gulp.series(
    'css:min',
    'css:gzip',
  )
);

gulp.task('js:gzip', function() {
  return gulp.src('./public/js/*.js')
    .pipe(gzip())
    .pipe(gulp.dest('./public/js'));
});

gulp.task('js:min', function() {
  return gulp.src('./public/js/*.js')
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./public/js_min'))
});

gulp.task('js:min:gzip', function() {
  return gulp.src('./public/js_min/*.js')
    .pipe(gzip())
    .pipe(gulp.dest('./public/js_min'))
});

gulp.task('js', gulp.series(
  gulp.parallel(
    'js:gzip',
    'js:min',
  ),
  'js:min:gzip',
  )
);

gulp.task('html', function() {
  return gulp.src(['./public/*.html'])
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gzip())
    .pipe(gulp.dest('./public'));
});

gulp.task('default', gulp.series(
  gulp.parallel(
    'css',
    'js',
    'html',
    )
  )
);

gulp.task('watch', function() {
  gulp.watch(['./public/js/*.js'], { ignoreInitial: false, events : ['change'] }, gulp.series('js'))
  gulp.watch(['./public/scss/*.scss'], { ignoreInitial: false, events : ['change'] }, gulp.series('css'))
  gulp.watch(['./public/*.html'], { ignoreInitial: false, events : ['change'] }, gulp.series('html'))
});
