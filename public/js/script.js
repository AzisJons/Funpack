(function (root) {
    'use strict';

    var header = get('header');
    var nav = get('nav');
    var iframe = get('iframe');
    var fd = get('.fd');
    var goTop = get('.go-top');

    if (!Element.prototype.matches) {
        Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
    }

    (function init() {
        sizeIframe();
    })();

    function toggleDescription(siblings) {
        var el = siblings[siblings.length -1];

        if (el.classList.contains('show')) {
            el.classList.remove('show');
            siblings[1].children[1].classList.add('hidden');
            siblings[1].children[0].classList.remove('hidden');

            setTimeout(function() { 
                if (!el.classList.contains('show')) {
                    el.style.display = 'none';
                }
            }, 800);

            
        } else {
            el.style.display = 'block';

            siblings[1].children[0].classList.add('hidden');
            siblings[1].children[1].classList.remove('hidden');

            setTimeout(function() { 
                if (el.style.display == 'block') {
                    el.classList.add('show');
                }
            }, 100);
        }
    }

    window.onscroll = function() {
        if (!document.body.classList.contains('kubilas-only')) {
            if (window.scrollY > 0) {
                header.classList.add('js-header-sticky');
            } else {
                header.classList.remove('js-header-sticky');
            }
        }

        if (window.scrollY > header.offsetHeight 
                && window.scrollY + window.innerHeight - 56 > fd.offsetTop + fd.offsetHeight) {
            goTop.classList.add('show');
        } else {
            goTop.classList.remove('show');
        }
    };

    window.addEventListener('resize', function() {
        sizeIframe();
    }, true);

    document.addEventListener('click', function (event) {

        if (event.target.parentNode.matches('.img')) {
            var siblings = event.target.parentNode.parentNode.children;
            toggleDescription(siblings);
            return;
        }

        if (event.target.matches('.more-info')) {
            var siblings = event.target.parentNode.children;
            toggleDescription(siblings);
            return;
        }

        if (event.target.parentNode.matches('nav')) {
            event.target.parentNode.classList.toggle('js-display');
            return;
        }

        if (event.target.matches('.menu-icon') || event.target.parentNode.matches('.menu-icon')) {
            nav.classList.toggle('js-display');
            return;
        }

        if (event.target.parentNode.matches('.go-top')) {
            window.scrollTo(0, 0);
            return;
        }

    }, false);

    function sizeIframe () {
        if (fd.clientWidth >= 1024) {
            iframe.width = '956px';
        } else {
            iframe.width = fd.clientWidth;
        }
    }

    // helpers
    function get(selector, scope) {
        scope = scope ? scope : document;
        return scope.querySelector(selector);
    };

    function getAll(selector, scope) {
        scope = scope ? scope : document;
        return scope.querySelectorAll(selector);
    };

    // exports to global
    root.get = get;
    root.getAll = getAll;

})(typeof self !== 'undefined' ? self : window);


