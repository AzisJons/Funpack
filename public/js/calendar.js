(function (root) {
    'use strict';

    var calendar1 = get('.calendar-1');
    var calendar2 = get('.calendar-2');

    // var DAYS = ['Pr', 'A', 'T', 'K', 'Pn', 'Š', 'S'];
    var MONTHS = ['Sausis', 'Vasaris',
        'Kovas', 'Balandis', 'Gegužė',
        'Birželis', 'Liepa', 'Rugpjūtis',
        'Rugsėjis', 'Spalis', 'Lapkritis',
        'Gruodis'
    ];

    var timestamp = new Date();

    var year = timestamp.getFullYear();

    var month = timestamp.getMonth();
    var nextMonth = month + 1;

    var currentDate = timestamp.getDate();

    (function init() {
        fillCalendar(calendar1, month, currentDate);
        fillCalendar(calendar2, nextMonth);
    })();

    function setYearMonth(el, year, monthNumber) {
        el.innerHTML = '<h3>' + year + ' ' + getMonthName(monthNumber) + '</h3>';
    }

    function getMonthName(monthNumber) {
        return MONTHS[monthNumber];
    }

    function fillCalendar(el, monthNumber, currentDate) {
        var yearMonthEl = get('.year-month', el);

        setYearMonth(yearMonthEl, year, monthNumber);
        draw(el, year, monthNumber, currentDate);
    }

    function draw(calendar, year, month, currentDate) {
        var startDay = new Date(year + '-' + (month + 1) + '-1').getDay(); // Note: Sun = 0
        var daysInMonth = new Date(year, month + 1, 0).getDate();
        var endDay = new Date(year + '-' + (month + 1) + '-' + daysInMonth).getDay();

        // Generate date squares
        var squares = [];
        // Blank squares before start of month
        if (startDay != 1) {
            for (var i=1; i<startDay; i++) { 
                squares.push('B');
            }
        }
        // Days of month
        for (var i=1; i<=daysInMonth; i++) { 
            var day = i
            if (currentDate && i < currentDate) {
                 day = day + 'b';
            }
            squares.push(day);
        }
        // Blank squares after end of month
        if (endDay != 0) {
            var blanks = 7 - endDay;
            for (var i=0; i<blanks; i++) { 
                squares.push('B'); 
            }
        }

        var html = '';
        var total = squares.length;
        for (var i=1; i<=total; i++) {
            var thisDay = squares[i-1];
            // unnecessary
            if (thisDay=='B') {
                html += '<span class="picker-d-blank"></span>';
            } else if (typeof thisDay === 'string') { 
                html += '<span class="picker-d-grey">' + thisDay.slice(0, -1) + '</span>';
            } else { 
                html += '<span class="picker-d">' + thisDay + '</span>';
            }
        }

        var daysEl = get('.days', calendar);
        daysEl.innerHTML = html;
    }

})(typeof self !== 'undefined' ? self : window);